from exchangelib import Credentials, Account, Configuration, FileAttachment
from datetime import datetime
import os


class Attachment:

    # Email credentials
    __email = 'bi@luxbp.com'
    __pw = 'Luxury2018'
    _jar_dir = os.path.dirname(__file__)

    def __init__(self, sender, attachmentname, location, subfolder, foldername):
        self.sender = sender
        self.attachmentname = attachmentname
        self.location = location
        self.credentials = Credentials(self.__email, self.__pw)
        self.config = Configuration(server='outlook.office365.com', credentials=self.credentials)
        self.account = Account(self.__email, credentials=self.credentials, autodiscover=False, config=self.config)
        self.folder = self.account.inbox / foldername
        self.subfolder = self.account.inbox / foldername / subfolder

    def save_attachment(self):
        for email in self.folder.all().order_by('-datetime_received')[:200]:
            if email.sender == self.sender:
                for attachment in email.attachments:
                    if attachment.name.startswith(self.attachmentname):
                        if isinstance(attachment, FileAttachment):
                            local_path = os.path.join(self._jar_dir, self.location, attachment.name)
                            attachmentname = attachment.name
                            emaildate = email.datetime_received
                            emaildate = emaildate.strftime('%m-%d-%Y')
                        with open(local_path, 'wb') as f:
                            f.write(attachment.content)
                            print('Saved attachment to', local_path)
                        file_location = local_path
                        email.is_read = True
                        email.save()
                        email.move(self.subfolder)
                        return file_location, attachmentname, emaildate
