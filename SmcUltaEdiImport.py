import pandas as pd
import csv
from sqlalchemy import create_engine
from pandas.io import sql as pandas_sql
import dataset
from stuf import stuf


class SmcUltaEdiImport:

    def __init__(self, fileloc, engine):
        self.fileloc = fileloc
        self.engine = engine

    def EdiImport(self):
        f = self.fileloc
        data = pd.read_csv(f, skiprows=4, index_col=None, usecols=['StoreNumber','ItemNumber', 'UPCNumber','BuyerNumber',
                                                                 'QS - Sold Qty','QA - Available Inventory Qty',
                                                                 'QP - On Order Not Yet Received Qty']).replace('"','', regex=True).replace("=",'', regex=True)

        df = pd.read_csv(f, engine='python', skiprows=1, nrows=1, usecols=['Buyer','Supplier', 'Report Start Date',
                                                                           'Report End Date'])
        start_date = df.iloc[0][2]
        end_date = df.loc[0][3]
        buyer = df.loc[0][0]
        supplier = df.loc[0][1]

        data.columns = ['store_number', 'item_number', 'upc_number', 'buyer_number', 'sold_qty', 'available_qty', 'on_order']

        data['start_date'] = pd.to_datetime(start_date)
        data['end_date'] = pd.to_datetime(end_date)
        data['buyer'] = buyer
        data['supplier'] = supplier
        data['store_number'] = data['store_number'].apply(pd.to_numeric, errors='coerce')
        data['key'] = data['store_number'].map(str) + '-' + data['item_number']
        data['sold_qty'] = data['sold_qty'].apply(pd.to_numeric, errors='coerce')
        data['available_qty'] = data['available_qty'].apply(pd.to_numeric, errors='coerce')
        data['on_order'] = data['on_order'].apply(pd.to_numeric, errors='coerce')
        data['sold_qty'].fillna(0, inplace=True)
        data['available_qty'].fillna(0, inplace=True)
        data['on_order'].fillna(0, inplace=True)

        db = dataset.connect(self.engine, schema='smc_ulta', row_type=stuf)
        edi = db['weekly_edi']
        rows = data.to_dict('records')
        edi.insert_many(rows)















