import pandas as pd
from datetime import date
from dateutil import parser as dateparser
from pandas.io import sql as pandas_sql


class SalonCentricImport:

    def __init__(self, fileloc, engine):
        self.fileloc = fileloc
        self.engine = engine

    def storeImport(self):

        file = self.fileloc
        df = pd.read_excel(file, sheet_name='Store')

        df = df.rename(columns={'Profit Center': 'Storage Location', 'Profit Center Code': 'Storage Location Code',
                                'Fiscal Month': 'Month'})

        new_location_code = []
        for x in df['Storage Location Code']:
            if len(str(x)) > 4:
                new = str(x)[2:]
            else:
                new = x
            new_location_code.append(new)

        month = []
        for x in df['Month']:
            y = x.replace('M','')
            y = y[:7]
            finaldate = dateparser.parse(y)
            finaldate = date(finaldate.year, finaldate.month, 1)
            month.append(finaldate)

        cols = ['LY Same Period Net Sls SD', 'LY Same Period Net Sls Qty', 'LY YTD Net Sls SD', 'LY YTD Net Sls Qty',
                'Net Sls Sd', 'Net Sales Qty', 'YTD Net Sls SD', 'Same Period Pct Chg Net Sls Sd',
                'Same Period Pct Chg Net Sls Qty', 'YTD Pct Chg Net Sls Sd', 'YTD Pct Chg Net Sls Qty']

        df[cols] = df[cols].apply(pd.to_numeric, errors='coerce')

        columns = ['month', 'distribution_channel', 'storage_location', 'storage_location_code', 'store_rank',
                   'ly_net_sales', 'ly_net_qty', 'ly_ytd_net_sales', 'ly_ytd_net_qty', 'net_sales', 'net_qty',
                   'ytd_net_sales', 'ytd_net_qty', 'pct_chg_net_sales', 'pct_chg_net_qty', 'ytd_pct_chg_net_sales',
                   'ytd_pct_chg_net_qty']

        df.columns = columns
        df.loc[:, 'month'] = month
        df.loc[:, 'storage_location_code'] = new_location_code
        df['brand'] = "IGK"

        engine = self.engine
        pandas_sql.to_sql(df, con=engine, schema='igk_saloncentric', name='monthly_store', if_exists='append',
                          index=False)


    def distributorImport(self):

        file = self.fileloc
        df = pd.read_excel(file, sheet_name='Sub Distributor')

        df = df.rename(columns={'Cust Lvl 4': 'Location', 'Fiscal Month': 'Month'})

        month = []
        for x in df['Month']:
            y = x.replace('M','')
            y = y[:7]
            finaldate = dateparser.parse(y)
            finaldate = date(finaldate.year, finaldate.month, 1)
            month.append(finaldate)

        cols = ['LY Same Period Net Sls SD', 'LY Same Period Net Sls Qty', 'LY YTD Net Sls SD', 'LY YTD Net Sls Qty',
                'Net Sls Sd', 'Net Sales Qty', 'YTD Net Sls SD', 'Same Period Pct Chg Net Sls Sd', 'Sub Distributor Net',
                'Same Period Pct Chg Net Sls Qty', 'YTD Pct Chg Net Sls Sd', 'YTD Pct Chg Net Sls Qty']

        df[cols] = df[cols].apply(pd.to_numeric, errors='coerce')

        columns = ['month', 'distribution_channel', 'location', 'ly_net_sales', 'ly_net_qty', 'ly_ytd_net_sales',
                   'ly_ytd_net_qty', 'net_sales', 'net_qty', 'ytd_net_sales', 'ytd_net_qty', 'sub_distributor_net',
                   'pct_chg_net_sales', 'pct_chg_net_qty', 'ytd_pct_chg_net_sales','ytd_pct_chg_net_qty']

        df.columns = columns

        df['month'] = month
        df['brand'] = "IGK"

        engine = self.engine
        pandas_sql.to_sql(df, con=engine, schema='igk_saloncentric', name='monthly_distributor', if_exists='append', index=False)





