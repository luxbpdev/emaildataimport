from SephoraSalesImport import SephoraSalesImport
from SaloncentricSalesImport import SalonCentricImport
from SmcUltaEdiImport import SmcUltaEdiImport
from Attachment import Attachment
from ExcelCsv import ExcelCsv
import argparse
from datetime import date
from datetime import datetime
from dateutil.relativedelta import relativedelta
from Dropbox import Dropbox
import os


mssql_engine = 'mssql+pymssql://arodriguez:dhPhHAlk@Bsdo|x4@luxbpdash.c8lx4xtix1pm.us-east-1.rds.amazonaws.com/LBP'
redshift_engine = 'redshift+psycopg2://lbp:Luxury2017@fivetran.cfyoxruhvrb0.us-east-1.redshift.amazonaws.com:5439/netsuite'

access_token = '32uj2fCNVLAAAAAAAAOiufk4nV8w3ih6IC4EOPan5RxC9hsWiJZFPuWYqbhW5oUk'
today = date.today()

parser = argparse.ArgumentParser(description='Runs import of excel sheet in email to database')
subparsers = parser.add_subparsers(help='commands')

weekly = subparsers.add_parser('weekly', help='Runs weekly process')
weekly.add_argument('weekly_run', help='Runs weekly process on given retailer. ie: sephora, ultaigkedi, jcpenney, ultasmcedi,')

monthly = subparsers.add_parser('monthly', help='Runs monthly process')
monthly.add_argument('monthly_run', help='Runs monthly process on given retailer. ie: sephora, saloncentric')

args = parser.parse_args()


def main():
    try:
        print(str(args))
        if hasattr(args, 'weekly_run') and args.weekly_run == 'sephora':
            att = Attachment('smart.reports@sephora.com', 'Weekly Sales by Location - IGK.xlsx', 'SephoraWeekly/', 'Weekly', 'Sephora')
            att_location, filename, att_date = att.save_attachment()
            weekly_excel = ExcelCsv(att_location)
            weekly_excel_location = weekly_excel.convert()
            transfer = Dropbox(access_token)
            file_to = '/Sephora Data/Weekly Sales By Location/Weekly Sales by Location %s.xlsx' % today
            curve_location = '/Apps/Curve Files/IGK Sephora Weekly Sales by Location Historical/Weekly Sales by Location %s.xlsx' % today
            transfer.upload_file(att_location, file_to)
            transfer.upload_file(att_location, curve_location)
            weekly = SephoraSalesImport(weekly_excel_location, mssql_engine)
            weekly.weeklyimport()
            print('Weekly report on Desktop has been completed')
        elif hasattr(args, 'weekly_run') and args.weekly_run == 'sephorasku':
            att = Attachment('smart.reports@sephora.com', 'Best Seller - IGK', 'SephoraWeekly/', 'Weekly SKU', 'Sephora')
            filepath, filename, att_date = att.save_attachment()
            date_time = datetime.strptime(att_date, '%m-%d-%Y')
            date_time = date_time - relativedelta(days=1)
            date_time = date_time.strftime('%m-%d-%Y')
            transfer = Dropbox(access_token)
            file_to = '/Sephora Data/Weekly Sales By SKU/Best Seller - IGK - Skincare ' + date_time + '.xlsx'
            curve_location = '/Apps/Curve Files/IGK Sephora Weekly Sales by SKU Historical/Best Seller - IGK - Skincare ' + date_time + '.xlsx'
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)
        elif hasattr(args, 'weekly_run') and args.weekly_run == 'ultaigkedi':
            att = Attachment('inbound@b2bgateway.org', 'IGKLLC-Ulta', 'IGKUltaWeekly/', 'Ulta IGK', 'EDI')
            filepath, filename, att_date = att.save_attachment()
            transfer = Dropbox(access_token)
            file_to = '/Data Engineering/Ulta IGK/Weekly EDI/%s' % filename
            curve_location = '/Apps/Curve Files/IGK Ulta Weekly EDI Historical/%s' % filename
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)
        elif hasattr(args, 'weekly_run') and args.weekly_run == 'jcpenney':
            att = Attachment('inbound@b2bgateway.org', 'IGKLLC-JCPenney852', 'JCPenney/', 'JCPenney', 'EDI')
            filepath, filename, att_date = att.save_attachment()
            transfer = Dropbox(access_token)
            file_to = '/Sephora Data/JCPenney EDI/%s' % filename
            curve_location = '/Apps/Curve Files/IGK JCPenney Weekly EDI Historical/%s' % filename
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)
        elif hasattr(args, 'weekly_run') and args.weekly_run == 'sephoracanadaedi':
            att = Attachment('inbound@b2bgateway.org', 'IGKLLC-SephoraCanada852', 'SephoraCanada/', 'SephoraCanada', 'EDI')
            filepath, filename, att_date = att.save_attachment()
            transfer = Dropbox(access_token)
            file_to = '/Sephora Data/SephoraCanada EDI Weekly/%s' % filename
            curve_location = '/Apps/Curve Files/IGK SephoraCanada Weekly EDI Historical/%s' % filename
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)
        elif hasattr(args, 'weekly_run') and args.weekly_run == 'sephoraedi':
            att = Attachment('inbound@b2bgateway.org', 'IGKLLC-Sephora852', 'SephoraUsEdi/', 'Sephora', 'EDI')
            filepath, filename, att_date = att.save_attachment()
            transfer = Dropbox(access_token)
            file_to = '/Sephora Data/SephoraUS EDI Weekly/%s' % filename
            curve_location = '/Apps/Curve Files/IGK SephoraUS EDI Weekly Historical/%s' % filename
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)
        elif hasattr(args, 'weekly_run') and args.weekly_run == 'ultasmcedi':
            att = Attachment('inbound@b2bgateway.org', 'SmithCultLLC-Ulta852', 'SmcUltaWeekly/', 'Ulta SMC', 'EDI')
            filepath, filename, att_date = att.save_attachment()
            x = SmcUltaEdiImport(filepath, redshift_engine)
            x.EdiImport()
            transfer = Dropbox(access_token)
            file_to = '/Data Engineering/Ulta SMC/EDI Reports/%s' % filename
            transfer.upload_file(filepath, file_to)
            os.remove(filepath)
        elif hasattr(args, 'monthly_run') and args.monthly_run == 'sephora':
            att = Attachment('smart.reports@sephora.com', 'IGK - Sales by Location.xlsx', 'SephoraMonthly/', 'Monthly', 'Sephora')
            monthly_att, filename, att_date = att.save_attachment()
            monthly_excel = ExcelCsv(monthly_att)
            monthly_location = monthly_excel.convert()
            transfer = Dropbox(access_token)
            datem = date(today.year, today.month, 1) - relativedelta(days=1)
            file_to = '/Sephora Data/Monthly Sales By Location/Monthly Sales by Location %s.xlsx' % datem
            curve_location = '/Apps/Curve Files/IGK Sephora Monthly Sales by Location Historical/Monthly Sales by Location %s.xlsx' % datem
            transfer.upload_file(monthly_att, file_to)
            transfer.upload_file(monthly_att, curve_location)
            monthly = SephoraSalesImport(monthly_location, mssql_engine)
            monthly.monthlyimport()
            print('Monthly report on Desktop has been completed')
        elif hasattr(args, 'monthly_run') and args.monthly_run == 'saloncentric':
            att = Attachment('scmonthlybrandreports@saloncentric.com', 'SC Monthly Brand Report - IGK',
                             'SaloncentricMonthly/', 'Completed', 'SalonCentric')
            filepath, filename, att_date = att.save_attachment()
            x = SalonCentricImport(filepath, redshift_engine)
            x.storeImport()
            x.distributorImport()
            transfer = Dropbox(access_token)
            monthend = date(today.year, today.month, 1) - relativedelta(months=1)
            file_to = '/SalonCentric Data/Completed Monthly Reports/SC Monthly Brand Report - IGK - %s.xlsx' % monthend
            curve_location = '/Apps/Curve Files/IGK SalonCentric Monthly Report Historical/SC Monthly Brand Report - IGK - %s.xlsx' % monthend
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)
        elif hasattr(args, 'monthly_run') and args.monthly_run == 'sephorasku':
            att = Attachment('smart.reports@sephora.com', 'IGK - Sales by Sku', 'SephoraMonthly/', 'Monthly SKU',
                             'Sephora')
            filepath, filename, att_date = att.save_attachment()
            date_time = datetime.strptime(att_date, '%m-%d-%Y')
            date_time = date_time - relativedelta(days=1)
            date_time = date_time.strftime('%m-%d-%Y')
            transfer = Dropbox(access_token)
            file_to = '/Sephora Data/Monthly Sales by SKU/IGK - Sales by Sku '+ date_time +'.xlsx'
            curve_location = '/Apps/Curve Files/IGK Sephora Monthly Sales by SKU Historical/IGK - Sales by Sku ' + date_time + '.xlsx'
            transfer.upload_file(filepath, file_to)
            transfer.upload_file(filepath, curve_location)
            os.remove(filepath)

    except Exception, e:
        print('- Start -')
        print str(e)
        print('- End -')


if __name__ == '__main__':
    main()
