import pandas as pd


class ExcelCsv:

    def __init__(self, file):
        self.file = file

    def convert(self):
        df = pd.read_excel(self.file)
        newfile = self.file.split('.')[0] + '.csv'
        df.to_csv(newfile, encoding='utf-8', index=False)
        newfile_location = newfile
        return newfile_location