from dateutil import parser as dateparser
from datetime import date
from dateutil.relativedelta import relativedelta
import pandas as pd
import csv
from sqlalchemy import create_engine
from pandas.io import sql as pandas_sql


class SephoraSalesImport:
    # Pushes CSV file to Database
    def __init__(self, fileloc, engine):
        self.fileloc = fileloc
        self.engine = engine

    def monthlyimport(self):
        f = open(self.fileloc)
        reader = csv.reader(f)
        rows = []
        num_rows = 0
        try:
            for row in reader:
                fixed = []
                for cell in row:
                    cell = cell.replace("=", "").replace("\"", "")
                    fixed.append(cell)
                rows.append(fixed)
                num_rows += 1
        except Exception as ex:
            print num_rows
            print ex
        brand = rows[1][0]
        csvdate = rows[-1][0]
        csv_date = dateparser.parse(csvdate)
        end_date = date(csv_date.year, csv_date.month, 1) - relativedelta(days=1)

        igk_data = rows[8:-3]

        igk_df = pd.DataFrame(igk_data)

        igk_df = igk_df[[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]]

        cols = ["Month End Sales Net $", "Sales % Growth  TY vs LY", "Month End AFS Retail $", "Month End Sales Net $ LY",
                "Month End AFS Retail $ LY", "End Inv_R %Chg TY vs LY", "Month End YTD Sales Net $", "Month End YTD Sales Net $ LY",
                "Sls % Change TY vs LY"]

        columns = ["Location Number", "Selling Type", "Location Desc", "Month End Sales Net $", "Sales % Growth  TY vs LY",
                   "Month End AFS Retail $", "Month End Sales Net $ LY", "Month End AFS Retail $ LY", "End Inv_R %Chg TY vs LY",
                   "Month End YTD Sales Net $", "Month End YTD Sales Net $ LY", "Sls % Change TY vs LY"]

        igk_df.columns = columns
        igk_df['Brand'] = brand
        igk_df['Month - Calendar'] = end_date
        igk_df[cols] = igk_df[cols].apply(pd.to_numeric, errors='coerce')
        engine = create_engine(self.engine)
        pandas_sql.to_sql(igk_df, con=engine, schema='dbo', name='Dashboard_Sell_Thru_MSBL', if_exists='append', index=False)

        print igk_df.dtypes

    def weeklyimport(self):
        f = open(self.fileloc)
        reader = csv.reader(f)
        rows = []
        num_rows = 0
        try:
            for row in reader:
                fixed = []
                for cell in row:
                    cell = cell.replace("=", "").replace("\"", "")
                    fixed.append(cell)
                rows.append(fixed)
                num_rows += 1
        except Exception as ex:
            print num_rows
            print ex

        brand = rows[1][0]
        csvdate = rows[6][3]
        date = dateparser.parse(csvdate)

        igk_data = rows[8:-1]

        igk_df = pd.DataFrame(igk_data)

        igk_df = igk_df[[1, 2, 3, 4, 5, 6, 7]]

        cols = ["Week End Sales Net $", "Week End Sales Net $ LY", "Week End Sales Net $ % Chg TY LY", "Week End YTD Sales Net $",
                "Week End YTD Sales Net $ LY"]
        columns = ["Region - Retailer", "Store Num - Store Name", "Week End Sales Net $", "Week End Sales Net $ LY", "Week End Sales Net $ % Chg TY LY", \
                   "Week End YTD Sales Net $", "Week End YTD Sales Net $ LY"]

        igk_df.columns = columns
        igk_df['Brand'] = brand
        igk_df['Week Ending'] = date
        igk_df[cols] = igk_df[cols].apply(pd.to_numeric, errors='coerce')

        engine = create_engine(self.engine)
        pandas_sql.to_sql(igk_df, con=engine, schema='dbo', name='Dashboard_Sell_Thru_WSBL', if_exists='append', index=False)

        print igk_df.dtypes
