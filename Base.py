import sys
import os

base_dir = os.path.dirname(__file__) or '.'

lib = os.path.join(base_dir, 'lib')
sys.path.insert(0, lib)
